package cz.uhk.fim.pro2.gta2.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import cz.uhk.fim.pro2.gta2.model.Box;
import cz.uhk.fim.pro2.gta2.model.Car;
import cz.uhk.fim.pro2.gta2.model.Heart;
import cz.uhk.fim.pro2.gta2.model.World;
import javafx.scene.image.Image;

public class GameCanvas extends JPanel {
	
	private World world;
	
	private BufferedImage imageCar;
	private BufferedImage imageBox;
	private BufferedImage imageHeart;
	
	public static final int ROAD_WIDTH = 500;


	public GameCanvas(World world) {
		super();
		this.world = world;
		
		try {
			imageCar = ImageIO.read(
						new File("assets/car.png"));
		}
		catch (IOException e){
			e.printStackTrace();
		}
	}
	
	public World getWorld() {
		return this.world;
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		int roadStartX = (getWidth() - ROAD_WIDTH) / 2;
		float ratioX = ROAD_WIDTH / World.WIDTH;
		float ratioY = ratioX;
		
	
		// 1. Draw grass
		g.setColor(new Color(100, 255, 100));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		// 2. Draw road
		g.setColor(new Color(30, 30, 30));
		g.fillRect(roadStartX, 0, ROAD_WIDTH, getHeight());
		
		// 3. Draw BROWN boxes
		List<Box> boxes = world.getBoxes();
		g.setColor(new Color(132, 96, 45));
		for (Box box : boxes) {
			g.fillRect((int)(roadStartX + (int) box.getX() * ratioX), getHeight() - (int) ((box.getY() + box.getHeight()) * ratioY), (int) (box.getWidth() * ratioX), (int) (box.getHeight() * ratioY));
		}
		
		// 4. Draw RED hearts
		List<Heart> hearts = world.getHearts();
		g.setColor(new Color(255, 0, 0));
		for (Heart heart : hearts) {
			g.fillRect((int)(roadStartX + (int) heart.getX() * ratioX), getHeight() - (int) ((heart.getY() + heart.getHeight()) * ratioY), (int) (heart.getWidth() * ratioX), (int) (heart.getHeight() * ratioY));
		}
		
		// 5. Draw White car
		Car car = world.getCar();
		g.drawImage(
				imageCar,
				roadStartX + (int) (car.getX() * ratioX),
				getHeight() - (int) ((car.getY() + car.getHeight()) * ratioY),
				(int) (car.getWidth() * ratioX),
				(int) (car.getHeight() * ratioY),
				null
		);
		/*
		g.setColor(new Color(255, 255, 255));
		g.fillRect(
				(int) (roadStartX + (int)car.getX() * ratioX), 
				getHeight() - (int) (car.getY() * ratioY), 
				(int) (car.getWidth() * ratioX), (int) (car.getHeight() * ratioY)
		);
		*/
	
	}
}
