package cz.uhk.fim.pro2.gta2.gui;



import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;

import cz.uhk.fim.pro2.gta2.interfaces.WorldListener;
import cz.uhk.fim.pro2.gta2.model.Box;
import cz.uhk.fim.pro2.gta2.model.Car;
import cz.uhk.fim.pro2.gta2.model.Heart;
import cz.uhk.fim.pro2.gta2.model.World;

public class MainFrame extends JFrame implements KeyListener, WorldListener {
	
	private GameCanvas gameCanvas;
	private World world;
	private JLabel labelSpeed;
	
	public MainFrame(World world) {
		super();
		
		this.world = world;
		this.world.addListener(this);
		// Frame Config
		setTitle("GTA");
		setSize(800, 600);
		setMinimumSize(new Dimension(550, 400));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//setResizable(false);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
	
		addKeyListener(this);
		
		labelSpeed = new JLabel("Rychlost: " + world.speed * 10 + " km/h");
		add(labelSpeed, BorderLayout.NORTH);
		
		gameCanvas = new GameCanvas(world);
		add(gameCanvas, BorderLayout.CENTER);
		
		
		setVisible(true);
	}
	
	public void revalidate() {
		super.revalidate();
		labelSpeed.setText("Rychlost: " + world.speed * 10 + " km/h");
		gameCanvas.repaint();
	}
	
	@Override
	public void keyPressed(KeyEvent e) {

	    int key = e.getKeyCode();
	    Car car = world.getCar();
	    
	    if (key == KeyEvent.VK_LEFT && car.getX() > 0) {
	    	car.updateX(-1);
	    }

	    if (key == KeyEvent.VK_RIGHT && car.getX() < world.WIDTH - 2f) {
	    	car.updateX(1);
	    }

	    if (key == KeyEvent.VK_UP && world.speed < 20f) {
	        //car.updateY(-2);
	        world.speed += 1f;
	    }

	    if (key == KeyEvent.VK_DOWN && world.speed > 1f) {
	        //car.updateY(2);
	        world.speed -= 1f;
	    }
	  
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCrashBox(Box box) {
		System.out.println("CRASH with " + box.toString());
	}

	@Override
	public void onCatchHeart(Heart heart) {
		System.out.println("CATCH heart " + heart.toString());
	}
}
