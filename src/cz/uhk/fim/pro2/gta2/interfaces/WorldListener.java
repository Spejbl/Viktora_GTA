package cz.uhk.fim.pro2.gta2.interfaces;

import cz.uhk.fim.pro2.gta2.model.Box;
import cz.uhk.fim.pro2.gta2.model.Heart;

public interface WorldListener {
	
	public void onCrashBox(Box box);
	public void onCatchHeart(Heart heart);
	
}
