package cz.uhk.fim.pro2.gta2.model;

import java.util.*;

import cz.uhk.fim.pro2.gta2.interfaces.WorldListener;

public class World {
	
	public static final float WIDTH = 20f;
	public static final float HEIGHT = 50f;
	
	private WorldListener worldListener;

	private Car car;
	
	private List<Heart> hearts;
	private List<Box> boxes;
	
	public float speed = 1f;
	
	public World(Car car) {
		this.car = car;
		this.hearts = new ArrayList<Heart>();
		this.boxes = new ArrayList<Box>();
	}

	public void addBox(Box box) {
		boxes.add(box);
	}

	public void addHeart(Heart heart) {
		hearts.add(heart);
	}
	
	public void init() {
		for(int i = 0; i < 5; i++) {
			double x = getRandomX();
			double y = WIDTH + (double)getRandomY();
			
			addBox(new Box((float)x,(float)y));
		}
		
		for(int i = 0; i < 1; i++) {
			double x = getRandomX();
			double y = HEIGHT + (double)getRandomY();
			
			addHeart(new Heart((float)x,(float)y));
		}	
	}
	
	public void update() {
		
		for (Box box : boxes) {
			box.y -= speed;
			if (box.y < 0) {
				box.x = getRandomX();
				box.y = World.HEIGHT;
			}
			if(car.collidesWith(box.getRectangle())) {
				// TODO SEBRAT SKORE
				//box.x = -100f;
				worldListener.onCrashBox(box);
			}
		}
		for (Heart heart : hearts) {
			heart.y -= speed;
			if (heart.y < 0) {
				heart.y = World.HEIGHT;
				heart.x = getRandomX();
			}
			if(car.collidesWith(heart.getRectangle())) {
				// TODO SEBRAT SKORE
				worldListener.onCatchHeart(heart);
			}
		}
	}
	
	private float getRandomX() {
		return (float)Math.random() * World.WIDTH;
	}
	
	private float getRandomY() {
		return (float)Math.random() * World.HEIGHT;
	}
	
	public Car getCar() {
		return this.car;
	}
	
	public List<Heart> getHearts() {
		return this.hearts;
	}
	
	public List<Box> getBoxes() {
		return this.boxes;
	}
	
	public void addListener(WorldListener worldListener) {
		this.worldListener = worldListener;
	}
	
	@Override
	public String toString() {
		String q = "World: \n";
		
		q += car + "\n";
		
		for(int i = 0; i < hearts.size(); i++) {
			q += hearts.get(i) + "\n";
		}
		for(int i = 0; i < boxes.size(); i++) {
			q += boxes.get(i) + "\n";
		}
			
		return q;		
	}
}
