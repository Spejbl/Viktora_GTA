package cz.uhk.fim.pro2.gta2.model;

import java.awt.Rectangle;

abstract public class GameObject {
	
	protected float x, y;
	

	public GameObject(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public float getX() {
		return this.x;
	}
	
	public float getY() {
		return this.y;
	}
	
	public void updateX(float x) {
		this.x += x;
	}
	
	public void updateY(float y) {
		this.y += y;
	}
	
	public boolean isOutOfWorld() {
		return y < 0;
	}
}
