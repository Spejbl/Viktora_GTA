package cz.uhk.fim.pro2.gta2.model;

import java.awt.geom.Rectangle2D;

public class Heart extends GameObject {
	
	private float width;
	private float height;
	
	private Rectangle2D.Float rectangle;

	public Heart(float x, float y) {
		super(x, y);
		this.width = 1f;
		this.height = 1f;
		rectangle = new Rectangle2D.Float(x, y, width, height);
	}
	
	public Rectangle2D.Float getRectangle() {
		this.rectangle.x = y;
		this.rectangle.y = y;
		return this.rectangle;
	}
	
	public float getHeight() {
		return this.height;
	}
	
	public float getWidth() {
		return this.width;
	}
	
	@Override
	public String toString() {
		String q = "Heart: ";
		q+= x + " " + y + "\n";
		return q;
	}
}
