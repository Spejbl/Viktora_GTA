package cz.uhk.fim.pro2.gta2.model;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

public class Car extends GameObject{
	private String name;
	
	private float width;
	private float height;
	
	private Rectangle2D.Float rectangle;
	
	public Car(String name, float x, float y) {
		super(x,y);
		this.name = name;
		this.width = 2f;
		this.height = 4f;
		rectangle = new Rectangle2D.Float(x, y, width, height);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Rectangle2D.Float getRectangle() {
		this.rectangle.x = y;
		this.rectangle.y = y;
		return this.rectangle;
	}
	
	public float getHeight() {
		return this.height;
	}
	
	public float getWidth() {
		return this.width;
	}
	
	@Override
	public String toString() {
		String q = "Car: ";
		q+= "name: " + name+ " " + x + " " + y + "\n";
		return q;
	}
	
	public boolean collidesWith(Rectangle2D.Float otherRectangle) {
		rectangle.y = y;
		rectangle.x = x;
		return this.rectangle.intersects(otherRectangle);
	}	

}
