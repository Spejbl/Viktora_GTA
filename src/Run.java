import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import cz.uhk.fim.pro2.gta2.gui.MainFrame;
import cz.uhk.fim.pro2.gta2.model.Car;
import cz.uhk.fim.pro2.gta2.model.World;

public class Run {
	static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {
		
		Car car = new Car("Shelby", 1f, 1f);
		World world = new World(car);
	    
	    	
		world.init();
		
		MainFrame mainFrame = new MainFrame(world);
		
		TimerTask timerTask = new TimerTask() {
			
			@Override
			public void run() {
				world.update();
				mainFrame.revalidate();
			}
		};
		
		Timer timer = new Timer();
	    timer.schedule(timerTask, 0, 40);
		
	}

}
